# Electron-Phonon Coupling and Electronic Friction Tutorial

This tutorial is focused on calculating electronic friction and electron-phonon coupling of molecules adsorbed on metal surfaces. Specifically, we are going to investigate the well-studied system of CO adsorbed on a Cu(100) surface.

https://fhi-aims-club.gitlab.io/tutorials/electronic-friction

Input files are available within this repo, in each specific Tutorial directory.