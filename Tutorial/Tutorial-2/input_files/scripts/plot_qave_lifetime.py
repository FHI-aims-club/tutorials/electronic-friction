from matplotlib.pyplot import legend
import numpy as np
import sys
import os
import glob
from pathlib import Path
import matplotlib
from numpy.testing._private.utils import jiffies
matplotlib.use('PDF')
import matplotlib.pyplot as plt
from matplotlib.ticker import (MultipleLocator, FormatStrFormatter,
                               AutoMinorLocator, MaxNLocator)

#plt.style.use('clb_publication')

prop_cycle = plt.rcParams['axes.prop_cycle']
colors = prop_cycle.by_key()['color']

#data = np.loadtxt('lifetimes.txt')

with open('lifetimes.txt', 'r') as f:
    file_lines = f.readlines()


print(len(file_lines))


fig, ax = plt.subplots(1, 1)
#for ik,k in enumerate(ks):
    #nk = int(k)
    #idx = np.argwhere(data[:,0]==k)[:,0]



#ks = data[:,0]
#q0 = data[:,-1]

q0 = []
q_ave = []
ks = []
for i in range(len(file_lines[1:])):
    data = np.fromstring(file_lines[i+1],sep=" ")
    nco = int(data[0])
    ks.append(nco)
    q0.append(data[-1])
    q_ave.append(np.array(1/np.average(1/data[-nco:])))


ks = np.array(ks)
q0 = np.array(q0)
q_ave = np.array(q_ave)

idx = np.argsort(ks)
ks = ks[idx]
q0 = q0[idx]
q_ave = q_ave[idx]

print(ks)
print("q_ave ----------")
print(q_ave)
print("q=0 ----------")
print(q0)
#ax.plot(ks,q0,color=colors[1],label="q0",linestyle="--",marker="o")# marker="None")
#ax.plot(ks,q_ave,color=colors[-1],linestyle="None")# marker="None")
ax.plot(ks,q_ave,color="mediumturquoise",linestyle="None",marker="o",markeredgecolor="teal",zorder=100)# marker="None")



ax.set_xticks([2,4,8,9,16,18])


ax.text(18.6,0.2,r"$\sigma=0.05$ eV", ha="right")

ax.text(16,2.1,r"Morin et al", color=colors[1],  ha="right", zorder=4)
ax.axhline(2,0,1,linestyle="-",color=colors[1])

ax.fill_between([0,20], [1,1], [3,3], color=colors[1],alpha=0.3 , zorder=-10)
ax.set_xlim(1,19)
ax.set_ylim(bottom=0, top=10.)
ax.set_xlabel(r'$N_{\mathrm{CO}}$',color='black')
#ax.set_ylabel(r'$\tau^{\bar{\mathbf{q}}}_{\mathrm{IS}}$ / ps',color='black')
ax.set_ylabel(r'$\tau^{\mathbf{q}=0}_{\mathrm{IS}}$ / ps',color='black')

ax.xaxis.set_minor_locator(MultipleLocator(1))

#ax.legend(fancybox=True,framealpha=0)
#plt.legend(ncol=3,handletextpad=0.15,columnspacing=0.6,fancybox=True,framealpha=0,handlelength=2,bbox_to_anchor=(0.5, 1.15), loc='center')
fig.set_figheight(2.2)
fig.set_figwidth(3.)
fig.savefig('qave_lifetime.pdf',transparent=True,bbox_inches='tight')
