# Purpose: Generate supercells of the surface
from ase.io import read,write
# from ase.build import surface, fcc111, hcp0001, sort
import numpy as np
import os

prim_cell = read('geometry.in')


for sc in [1,2,3,4]:
    # Create subdirectory
    supercell = prim_cell*(sc,sc,1)


    sub_dir = '{:02d}'.format(sc)
    os.mkdir(sub_dir)

    write(sub_dir+'/geometry.in', supercell, format='aims')