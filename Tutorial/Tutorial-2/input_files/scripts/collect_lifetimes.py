from matplotlib.pyplot import legend
import numpy as np
import sys
import os
import glob
from pathlib import Path
import matplotlib
from numpy.testing._private.utils import jiffies
matplotlib.use('PDF')
import matplotlib.pyplot as plt
from matplotlib.ticker import (MultipleLocator, FormatStrFormatter,
                               AutoMinorLocator, MaxNLocator)

output_dirs = sys.argv[1:]

f = open("lifetimes.txt", "w")
f.write("# N_CO, Perturbation / eV, Lifetimes flattened / ps\n")


pe = 0.252
for s,output_dir in enumerate(output_dirs):
    #for s,sigma in enumerate(sigmas):
    
    k = int(output_dir.split('/')[0])


        

    data = np.loadtxt(output_dir+"relax_rate")
    lifetimes = np.split(data,2)[1]



    lf_str = ' '.join(map(str, lifetimes.flatten()))


    f.write(str(k**2)+" "+str(pe)+" "+lf_str+"\n")



            


f.close()

