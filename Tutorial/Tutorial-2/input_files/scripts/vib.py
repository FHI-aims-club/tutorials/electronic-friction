#!/usr/bin/python
from ase.atoms import Atoms, Atom
from ase.constraints import FixInternals, FixAtoms
from ase.calculators.aims import Aims
from ase.vibrations import Vibrations
import numpy as np
from ase.io import write, read, PickleTrajectory


# aims settings
aims_dir = '/home/chem/msrvhs/software/aims/FHIaims/'
species_dir = aims_dir+'species_defaults/defaults_2020/'+'light/'
run_command = 'srun '+aims_dir+'build_debug/aims.240314.scalapack.mpi.x > aims.out'

# Read geometry
geometry_file='start.in'
structure=read(geometry_file)

# Vibrating atoms
#vib_indices = [16,17,18,19]

vib_indices = [i for i,e in enumerate(structure) if e.symbol == 'C' or e.symbol =='O']
#vib_indices = [a.symbol == 'C' for a in structure or a.symbol == 'O' for a in structure]
#C_index = [i for i, e in enumerate(structure) if e.symbol == 'C']
#O_index = [i for i, e in enumerate(structure) if e.symbol == 'O']
#vib_indices = C_index + O_index
#vib_indices.sort()
print(vib_indices)
# Constaints
c = FixAtoms(mask=[a.symbol == 'Cu' for a in structure])
structure.set_constraint(c)
structure.set_pbc([True,True,True])

# Calculator
calc=Aims(xc='PBE',
          aims_command=run_command,
          species_dir=species_dir,
          occupation_type = ['gaussian',0.1],
          sc_iter_limit = 100,
          #spin = 'collinear',
          relativistic = ['atomic_zora','scalar'],
          #default_initial_moment = 0,
          sc_accuracy_etot=1e-6,
          sc_accuracy_eev=1e-3,
          sc_accuracy_rho=1e-5,
          sc_accuracy_forces=1e-4,
          #load_balancing = True,
          k_grid = [32,32,1],
          #restart_aims='wvfn.dat',
          use_dipole_correction='.true.',
          #collect_eigenvectors='.false.',
          )

structure.set_calculator(calc)

# Vibrations
vib = Vibrations(structure,indices=vib_indices)
vib.run()
vib.summary()
vib.write_jmol()

modes=vib.modes
#Always exclude rotations and translations....ALL 0 frequencies must be excluded
#vibdata=[vib.hnu[6:],modes[6:,:]]
vibdata=[vib.hnu[:],modes[:]]#,seed_init=1)

for i in range(len(vib.hnu)):
    string = ''
    mode = vib.get_mode(i)[vib_indices]
    for j in range(len(vib.hnu)):
        string += ' {0:14.8f} '.format(modes[i,j])
        #string += ' {0:14.8f} '.format(mode.flatten()[j])
    print(string)


#m = vib.atoms.get_masses()
#im = np.repeat(m[vib.indices]**-0.5,3)
#D = im[:, None]*vib.H*im
#for i in range(6):
#  str=''
#  for j in range(6):
#    str+= '{0:16.8f}'.format(D[i,j])
#  print str

#from ase.vibrations.populate_vibrations import populate_vibrations

#structure = structure[:24]

#md_atoms = populate_vibrations(structure, vibdata=vibdata, T=400.0, runmode='stat')

#print md_atoms.positions
#print md_atoms.get_velocities()


#electronicenergy =-84838.29594669

#vib_energies = vib.get_energies()
#vib_energies[0]=0.0015
#from ase.thermochemistry import IdealGasThermo,HarmonicThermo

#thermo = HarmonicThermo(vib_energies=vib_energies,
#                        electronicenergy=electronicenergy)#,
#                        atoms=structure)#,
                        #geometry='nonlinear',
                        #symmetrynumber=1, spin=0)

#G = thermo.get_gibbs_energy(temperature=400.0)

#print thermo.vib_energies

#thermo = IdealGasThermo(vib_energies=vib_energies,
#                        electronicenergy=electronicenergy,
#                        atoms=structure,
#                        geometry='nonlinear',
#                        symmetrynumber=1, spin=0)

#G = thermo.get_gibbs_energy(temperature=400.0, pressure = 0.0001)

