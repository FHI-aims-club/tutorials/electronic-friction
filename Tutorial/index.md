# Electron-Phonon Coupling and Electronic Friction

**Prepared by Connor L. Box, Mariana Rossi and Reinhard J. Maurer**



## Summary of the tutorial

The tutorial is organised as follows:

[**Part 1**](./Tutorial-1/README.md) introduces theory of electron-phonon coupling and electronic friction  
[**Part 2**](./Tutorial-2/README.md) calculates the vibrational lifetime of CO adsorbed on a Cu(100) surface  
