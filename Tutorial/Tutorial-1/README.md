# Electron-Phonon Coupling and Electronic Friction

We refer the interested reader to two implementation papers of electronic friction in FHI-aims:

[1] [Box, C. L., Stark, W. G., & Maurer, R. J. (2023). Ab initio calculation of electron-phonon linewidths and molecular dynamics with electronic friction at metal surfaces with numeric atom-centred orbitals. Electronic Structure, 5(3), 035005.](https://iopscience.iop.org/article/10.1088/2516-1075/acf3c4/meta)    
[2] [Maurer R. J., Askerka M., Batista V. S., Tully J. C. (2016). Ab initio tensorial electronic friction for molecules on metal surfaces: Nonadiabatic vibrational relaxation. Physical Review B, 94(11):115432.](https://link.aps.org/doi/10.1103/PhysRevB.94.115432)

## Theory overview

In metallic systems, electronic and vibrational dynamics can be strongly coupled due to the insufficient energy scale separation between low-lying electronic excitations and vibrational motion. This coupling also affects the reactive dynamics of molecules adsorbed on metallic surfaces. Because of the infinitesimal bandgap in metallic surfaces, adsorbate dynamics may be affected by nonadiabatic effects, with experimentally measurable consequences. This includes the ultrafast decay of adsorbate vibrational excitation, resulting in picosecond-scale vibrational lifetimes of molecular adsorbates. Ultrashort adsorbate lifetimes have been observed for systems such as CO on Cu(100) and Pt(111). Atomic and molecular beam scattering experiments of hydrogen atoms and diatomics such as nitric oxide exhibit highly vibrationally inelastic scattering behavior that arises from electron–hole pair excitations. Similarly, ultrafast time-resolved spectroscopy measurements have shown efficient activation of molecular motion and desorption driven by hot electron excitation. Beyond capturing the fundamental dynamics at surfaces, hot electrons are also relevant in the context of light-controlled chemistry and catalysis.

The vibrational linewidth ($\gamma$) of a phonon mode, $\nu$, at momentum $\mathbf{q}$ and frequency, $\omega$, can be related to the relaxation rate ($\Gamma$) and the electronic friction tensor $\mathbf{\Lambda}$ through the following relationship:

$$
\begin{align*}
    \gamma_{\mathbf{q}\nu} = \hbar \Gamma_{\mathbf{q}\nu} = \hbar \left[ \tilde{\mathbf{u}}_{\mathbf{q}\nu} \mathbf{\Lambda}^{\mathbf{q}}(\hbar\omega_{\mathbf{q}\nu})\tilde{\mathbf{u}}^T_{\mathbf{q}\nu}  \right]
\end{align*}
$$

Where $\mathbf{u}$ are the normal mode displacement vectors. In this manner, the electronic friction tensor is in a Cartesian representation.

The electronic friction tensor is the direct property calculated by FHI-aims, through either finite-difference or density functional perturbation theory methods. This electronic friction tensor in Cartesian representation can then be projected along the normal mode displacement vectors to obtain vibrational linewidths.

Alternatively, the electronic friction tensor can be used within molecular dynamics with electronic friction schemes to propagate semi-classical dynamics (see referred publications for more information.)

FHI-aims offers multiple ways to evaluate electronic friction, the one we will use in this tutorial is the following:

$$
\begin{align*}
\Lambda_{a\kappa,a'\kappa'}^{\mathbf{q},\mathrm{Ib}}(\hbar\omega) = \pi\hbar \sum_{\sigma mn}\int \frac{d\mathbf{k}}{\Omega_{BZ}}  \tilde{g}^{a\kappa}_{\sigma mn} (\mathbf{k},\mathbf{q})   (\tilde{g}^{a'\kappa'}_{\sigma mn})^{*} (\mathbf{k},\mathbf{q}) \\
(f_{\sigma n\mathbf{k}}-f_{\sigma m\mathbf{k}+\mathbf{q}}) \frac{\delta(\epsilon_{\sigma m\mathbf{k}+\mathbf{q}}-\epsilon_{\sigma n\mathbf{k}}-\hbar\omega)}{\epsilon_{\sigma m\mathbf{k}+\mathbf{q}}-\epsilon_{\sigma n\mathbf{k}}}.
\end{align*}
$$

Where $a\kappa$ are the atomic coordinates in Cartesian representation, $g$ are the electron-phonon matrix elements, and $f$ are the Fermi occupation factors, and $\epsilon$ are the eigenvalues for eigenstates with index $m$ and $n$ and spin index $\sigma$.

The $\delta$ function is replaced by typically a Gaussian broadening function with user-defined width. This has an effect on the magnitude of the electronic friction, and, hence, the vibrational linewidth. The effect of this must carefully be analysed by the user, as finite widths are required to achieve robust prediction. The choice of sigma and how dense the $\mathbf{k}$-mesh needs to be depends on the system and the employed unit cell

## Phonon dephasing, momenta etc

Lets consider the possible vibrations of an adsorbate overlayer at a surface, for example, carbon monoxide as pictured below:"

<img src="motion2.png" alt="Render of CO/Cu(100) surface" width="500"/>

We can easily model the in-phase coherent motion of the vibration of the adsorbated ($\mathbf{q}=0$, case (b) above) within periodic boundary conditions. Each unit cell contains one adsorbate molecule. Currently, (b) is the only case that is directly supported within FHI-aims. To model cases (a) and (c), which are the case of an aperiodic, isolated vibration and the maximally out-of-phase vibration ($\mathbf{q}=q_\mathrm{max}$) respectively, requires a slightly different approach using supercells."

By using supercells, we now have two or more adsorbate molecules in the unit cell, and so we are capturing higher order phonon motion. For example, with two adsorbate molecules in the unit cell, we can now capture two phonon momenta ($\mathbf{q}$):

<img src="supercells.png" alt="Render of CO/Cu(100) surface" width="500"/>

As with the primitive unit cell, we can capture the inphase phonon motion ($\mathbf{q}=0$), but now we can capture the completely out-of-phase motion ($\mathbf{q}=q_{\mathrm{max}}$). Increasing the size of the supercell further samples more $\mathbf{q}$ points within this range. Thus, we can capture motion of arbitary $\mathbf{q}$ within an implementation limited to direct $\mathbf{q}=0$ calculations.

To capture case (a) requires an extra step. Case (a) breaks periodicity, the vibration exists only in the principal unit cell, the motion is completely aperiodic. We can capture this by a sum over all possible $\mathbf{q}$ values:

$$
\begin{align*}
\bar{\Gamma}_\nu=\frac{1}{N_{\mathbf{q}}} \sum_{\mathbf{q}} \Gamma_{\nu, \mathbf{q}}
\end{align*}
$$

We will carry these procedures out in the [Part 2](https://electronic-friction-fhi-aims-club-tutorials-f63919c64104ee21276.gitlab.io/Tutorial-2/)